import snscrape.modules.twitter as sntwitter
from flask import Flask, request
import datetime

from mongo import get_database
dbname = get_database()
tweets_collection = dbname['tweets']
twitterUsers_collection = dbname['twitterUsers']


app = Flask(__name__)

@app.post("/")
def find_tweets():
    inputtedData = request.json
    conversation_id = inputtedData['conversation_id'] if 'conversation_id' in inputtedData else None
    print("[conversation_id] ", conversation_id)
    # find "since_id"
    tweet = find_last_tweet_based_on_conversation_id(conversation_id)
    since_id = tweet["tweetId"] if tweet and "tweetId" in tweet else None

    # write query
    conversation_id_query = f'conversation_id:{conversation_id}' if conversation_id else ""
    since_id_query = f'since_id:{since_id}' if since_id else ""

    print("SINCE_ID", conversation_id_query)
    for i, tweet in enumerate(sntwitter.TwitterSearchScraper(f'{conversation_id_query} {since_id_query} filter:safe').get_items()):
        print(f"[{conversation_id}]", i, tweet.id)
        if i > 500:
            break

        store_tweet(tweet)
        store_user(tweet.user)
    
    return { "status": "done" }
    

def store_tweet(tweet):
    tweet_doc = {
        "tweetId" : str(tweet.id),
        "authorId" : str(tweet.user.id),
        "conversationId" : str(tweet.conversationId),
        "referencedTweets" : [{ "type": "replied_to", "id": str(tweet.inReplyToTweetId) }],
        "text" : tweet.renderedContent,
        "tweetCreatedAt" : tweet.date,
        "createdAt": datetime.datetime.now(),
        "updatedAt": datetime.datetime.now()
    }

    tweets_collection.update_one({ "tweetId": tweet_doc["tweetId"] }, { "$set": tweet_doc}, upsert=True)


def store_user(user):
    user_doc = {
        "tracked": False,
        "profileImageUrl": user.profileImageUrl,
        "protected": user.protected,
        "verified": user.verified,
        "username": user.username,
        "name": user.displayname,
        "description": user.renderedDescription,
        "userCreatedAt": user.created,
        "twitterUserId": str(user.id),
    }

    # TODO: don't update tracked users
    twitterUsers_collection.update_one({ "twitterUserId": user_doc["twitterUserId"], "tracked": False } , { "$set": user_doc }, upsert=True)


def find_last_tweet_based_on_conversation_id(conversation_id: str):
    lastTweet = tweets_collection.aggregate([
      { "$match": { "conversationId": conversation_id } },
      { "$sort": { "tweetId": -1 } }, 
      { "$limit": 1 },
    ])
    listLastTweet = list(lastTweet)

    if len(listLastTweet) > 1:
        return listLastTweet[0]
    else:
        return None


if __name__ == '__main__':
      app.run(host='0.0.0.0', port=2000)
